# Git Summary

Git is a version control system that makes software management easier. You can collaborate with various peers through same codebase creating it's local copies on their system. You can track each and every change in your code ever committed, so that you can revert back to any step. Git is simpler to use and lightweight.

## Installation

#### 1. Ubuntu

`sudo apt-get install git`

For other Linux distribution [check this](https://git-scm.com/download/linux).

#### 2. Windows

Download git from [here](https://git-scm.com/download/win) and install within few clicks.

#### 3. Mac

Mac comes with pre-installed git.
If in case you delete and want to re-download use `brew install git`

## Git Structure

#### File Stages

1. Modified: This is more like saving a file(as usually you do with ctrl+s). You make changes to your file but don't commit.
2. Staged: In this you add/remove/move files which you have modified and are ready to commit.
3. Commit: In this you commit all the staged files to your local git(consider like taking backup's).

#### Software Stages

1. Workspace: All changes made in code through editors(notepad, gedit) and stored in tree of repository.
2. Staging: The staged files are stored in tree of repository.
3. Local Repository: Whenever you commit your files, all the changes are saved to local repository.
4. Remote Repository: This is a copy of your project stored on some server. Your all local repository changes can be pushed to remote repository.

![Git Structure](https://www.edureka.co/blog/wp-content/uploads/2016/11/Git-Architechture-Git-Tutorial-Edureka-2.png)

## Git Commands

#### Clone
Cloning is downloading a copy of that repository(project folder).

>`git clone <project-clone-url>`

#### Branch
By default the branch is master branch. If you are experimenting with your code and you do not want it to affect your main code, you should create a branch and work under it.

>`git checkout master`

>`git checkout -b <branch-name>`

#### Merge
After your successful experimentation, you can merge your created branch with the master branch.

>`git merge <branch-name>`

#### Add
Adding all untacked files that are ready to commit.

>`git add .`  _#Here "." adds all all files changed._

>`git add <file-name>`

#### Commit
When you have completed your tasks with no errors you can commit all staged files to local repository.

>`git commit -sv` _#Write a message of changes committed_

#### Push
You can push your local repository changes to remote repository Whenever your code is ready and fully executional.

>`git push origin <branch-name>`